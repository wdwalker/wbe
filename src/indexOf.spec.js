import indexOf from './indexOf';

const goodArrayEven = [-23, -2, 0, 2, 5, 20];
const goodArrayOdd = [-23, -2, 1, 5, 20];
const badArray = [6, 5, 4, 2];

describe('indexOf', () => {
  it('should throw a TypeError', () => {
    expect(() => {
      indexOf('banana');
    }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => {
      indexOf('banana', 'monkey');
    }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => {
      indexOf(undefined);
    }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => {
      indexOf(null);
    }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => {
      indexOf(undefined, null);
    }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => {
      indexOf([], 'banana');
    }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => {
      indexOf([], null);
    }).toThrowError(TypeError);
  });
  it('should return -1', () => {
    expect(indexOf([], 22)).toEqual(-1);
  });
  it('should return -1', () => {
    expect(indexOf([], 22)).toEqual(-1);
  });
  it('should return -1', () => {
    expect(indexOf(goodArrayEven, NaN)).toEqual(-1);
  });
  it('should return -1', () => {
    expect(indexOf(goodArrayEven, Infinity)).toEqual(-1);
  });
  goodArrayEven.forEach((val, n) => {
    it(`should return return ${n}`, () => {
      expect(indexOf(goodArrayEven, val)).toEqual(n);
    });
  });
  goodArrayEven.forEach((val) => {
    it('should return return -1', () => {
      expect(indexOf(goodArrayEven, val + 1)).toEqual(-1);
    });
  });
  goodArrayOdd.forEach((val, n) => {
    it(`should return return ${n}`, () => {
      expect(indexOf(goodArrayOdd, val)).toEqual(n);
    });
  });
  goodArrayOdd.forEach((val) => {
    it('should return return -1', () => {
      expect(indexOf(goodArrayOdd, val + 1)).toEqual(-1);
    });
  });
  it('should return return -1', () => {
    expect(indexOf(badArray, 3)).toEqual(-1);
  });
});

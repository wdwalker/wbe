/* eslint-disable no-bitwise */

'use-strict';

const ERROR_STR =
  'board must be a string of length 81 containing only the characters "1"-"9" and ".".';

// Indicates all numbers 1-9 are in the given row, column, or box
const ALL_SET = 0x3fe;

/**
 * Simple Sudoku puzzle solver.
 * @type {Object}
 */
class Sudoku {
  /**
   * Create a new Sudoku puzzle with the given board.
   * @param  {string} board a string holding the board in row-major order; valid
   * characters are '1'-'9' and the character '.' to indicate an empty cell.
   * @return {Sudoku} a new Sudoku instance
   */
  constructor(board) {
    if (typeof board !== 'string') {
      throw new TypeError(ERROR_STR);
    }
    if (!board.match(/[1-9.]{81}/)) {
      throw new Error(ERROR_STR);
    }

    // The board converted to an array of ints; 0 is an empty cell
    this.board = [];

    // We keep a mask for each row, column, and box of 9 cells. A set bit in
    // a mask indicates the associated number (as represented by bit position)
    // is somewhere in the given row, column, or box.
    this.rowTenants = [];
    this.colTenants = [];
    this.boxTenants = []; // in row-major order

    let row;
    let col;
    let box;
    board.split('').forEach((c, i) => {
      const val = parseInt(c, 10);
      this.board[i] = val || 0;
      if (this.board[i] > 0) {
        row = Math.floor(i / 9);
        col = i % 9;
        box = (3 * Math.floor(row / 3)) + Math.floor(col / 3);
        if ((this.rowTenants[row] | this.colTenants[col] | this.boxTenants[box])
            & (1 << this.board[i])) {
          throw new Error('Illegal board');
        }
        this.checkIn(row, col, box, this.board[i]);
      }
    });
  }

  /**
   * Record the given [row, col] as holding the given value.
   * @param  {int} row the row in the board
   * @param  {int} col the column in the board
   * @param  {int} box the box of 9 cells in the board
   * @param  {int} val the value to place at [row, col]
   * @return {undefined}
   */
  checkIn(row, col, box, val) {
    const bit = 1 << val;
    this.rowTenants[row] |= bit;
    this.colTenants[col] |= bit;
    this.boxTenants[box] |= bit;
  }

  /**
   * Forget val as ever having occupies the given [row, cell]
   * @param  {int} row the row in the board
   * @param  {int} col the column in the board
   * @param  {int} box the box of 9 cells in the board
   * @param  {int} val the value to clear at [row, col]
   * @return {undefined}
   */
  checkOut(row, col, box, val) {
    const mask = ~(1 << val);
    this.rowTenants[row] &= mask;
    this.colTenants[col] &= mask;
    this.boxTenants[box] &= mask;
  }

  /**
   * Returns true if placing the given val at [row, col] is a legal Sudoku move.
   * @param  {int} row the row in the board
   * @param  {int} col the column in the board
   * @param  {int} box the box of 9 cells in the board
   * @param  {int} val the value to place at [row, col]
   * @return {Boolean} true if placing val at [row, col] is legal
   */
  isLegalValue(row, col, box, val) {
    return !((this.rowTenants[row] | this.colTenants[col] | this.boxTenants[box]) & (1 << val));
  }

  /**
   * Returns true if the board is legal.
   * @return {Object} this Sudoku instance if the board is legal.
   */
  isLegalBoard() {
    let row;
    let col;
    let box;
    for (let i = 0; i < this.board.length; i += 1) {
      row = Math.floor(i / 9);
      col = i % 9;
      box = (3 * Math.floor(row / 3)) + Math.floor(col / 3);
      if (this.rowTenants[row] !== ALL_SET
          || this.colTenants[col] !== ALL_SET
          || this.boxTenants[box] !== ALL_SET) {
        return null;
      }
    }
    return this;
  }

  /**
   * Solve the Sudoku puzzle. Our toString and toBoard methods will emit the solution.
   * This is a recursive function that performs a backtracking algorithm.
   * @param  {Number} [cell=0] the cell at which we are starting
   * @return {Object} this Sudoku instance if we were able to solve the puzzle; otherwise null
   */
  solve(cell = 0) {
    // Find the next empty cell
    let empty = cell;
    while (this.board[empty] !== 0 && empty < 81) {
      empty += 1;
    }

    // If there are no more empty cells, we're done.
    if (empty >= 81) {
      return this;
    }

    // Otherwise, start trying candidates in the current empty cell, plodding
    // forward on legal moves and backtracking on failure
    const row = Math.floor(empty / 9);
    const col = empty % 9;
    const box = (3 * Math.floor(row / 3)) + Math.floor(col / 3);
    for (let candidate = 1; candidate <= 9; candidate += 1) {
      if (this.isLegalValue(row, col, box, candidate)) {
        this.board[empty] = candidate;
        this.checkIn(row, col, box, candidate);
        if (this.solve(empty + 1)) {
          return this;
        }
        this.checkOut(row, col, box, candidate);
        this.board[empty] = 0;
      }
    }
    return null;
  }

  /**
   * Return a string holding ASCII art for the current board state.
   * @return {string} a string holding ASCII art for the current board state.
   */
  toBoard() {
    let str = '';
    let val;
    for (let row = 0; row < 9; row += 1) {
      if (row > 0 && !(row % 3)) {
        str += '-------+-------+-------\n';
      }
      for (let col = 0; col < 9; col += 1) {
        if (col > 0 && !(col % 3)) {
          str += ' |';
        }
        val = this.board[(row * 9) + col];
        str += val ? ` ${val}` : '  ';
      }
      str += '\n';
    }
    return str;
  }

  /**
   * Return a string holding the current board state in row-major order.
   * The character '.' indicates an empty cell.
   * @return {string} a string holding the current board state in row-major order.
   */
  toString() {
    return this.board.map(cell => cell || '.').join('');
  }
}

export default Sudoku;

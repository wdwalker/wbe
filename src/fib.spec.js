import fib from './fib';

const seq = [1, 1, 2, 3, 5, 8, 13, 21];

describe('fib', () => {
  it('should throw a RangeError', () => {
    expect(() => { fib(1.2); }).toThrowError(RangeError);
  });
  it('should throw a RangeError', () => {
    expect(() => { fib(-1); }).toThrowError(RangeError);
  });
  it('should throw a RangeError', () => {
    expect(() => { fib(-42); }).toThrowError(RangeError);
  });
  it('should throw a RangeError', () => {
    expect(() => { fib(NaN); }).toThrowError(RangeError);
  });
  it('should throw a RangeError', () => {
    expect(() => { fib(Infinity); }).toThrowError(RangeError);
  });
  it('should throw a TypeError', () => {
    expect(() => { fib(null); }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => { fib(undefined); }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => { fib('banana'); }).toThrowError(TypeError);
  });
  seq.forEach((val, n) => {
    it(`should return return ${val}`, () => {
      expect(fib(n)).toEqual(val);
    });
  });
});

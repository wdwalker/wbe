/**
 * Returns the index of val in the given array, arr.
 * @param  {int[]} arr sorted array of integers
 * @param  {int} val the value to find
 * @return {int} index of val in arr or -1 if val is not in the array
 */
const indexOf = (arr, val) => {
  if (!Array.isArray(arr)) {
    throw new TypeError('Must be given a sorted array of values');
  }
  if (typeof val !== 'number') {
    throw new TypeError(`Not a number: ${val}`);
  }
  if (arr.length === 0 || Number.isNaN(val)) {
    return -1;
  }

  // We're doing a binary search and should be able
  // to find the value in log(n) time.
  let lower = 0;
  let upper = arr.length - 1;
  while (lower <= upper) {
    const middle = Math.floor((lower + upper) / 2);
    if (val > arr[middle]) {
      lower = middle + 1;
    } else if (val < arr[middle]) {
      upper = middle - 1;
    } else {
      return middle;
    }
  }

  return -1;
};

export default indexOf;

/**
 * Return the number of bits set to 1 in the given integer.
 * @param  {number} n the non-negative integer
 * @return {int} the number of bits set to 1
 */
const nbits = (n) => {
  if (typeof n !== 'number') {
    throw new TypeError(`Not a number: ${n}`);
  }
  if (n > 0xFFFFFFFF || !Number.isInteger(n)) {
    throw new RangeError(`Invalid value ${n}. Must be a 32-bit integer.`);
  }

  /* eslint-disable no-bitwise */

  let count = 0;
  let m = n;
  while (m) {
    count += m & 1;
    m >>>= 1;
  }

  return count;
};

export default nbits;

/**
 * Return the nth number in the fibonacci sequence, where 0 is the first number.
 * @param  {int} n the index to find in the fibonacci sequence
 * @return {int} the fibonacci number at index n
 */
const fib = (n) => {
  if (typeof n !== 'number') {
    throw new TypeError(`Not a number: ${n}`);
  }
  if ((n < 0) || (n % 1 !== 0)) {
    throw new RangeError(`Invalid index ${n}. Must be an integer >= 0.`);
  }

  let tmp;
  let m2 = 0;
  let m1 = 1;
  for (let i = 0; i < n; i += 1) {
    tmp = m1;
    m1 = tmp + m2;
    m2 = tmp;
  }

  return m1;
};

export default fib;

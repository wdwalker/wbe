import nbits from './nbits';

describe('nbits', () => {
  it('should throw a RangeError', () => {
    expect(() => {
      nbits(2.2);
    }).toThrowError(RangeError);
  });
  it('should throw a RangeError', () => {
    expect(() => {
      nbits(Number.MAX_SAFE_INTEGER);
    }).toThrowError(RangeError);
  });
  it('should throw a RangeError', () => {
    expect(() => {
      nbits(NaN);
    }).toThrowError(RangeError);
  });
  it('should throw a RangeError', () => {
    expect(() => {
      nbits(Infinity);
    }).toThrowError(RangeError);
  });
  it('should throw a TypeError', () => {
    expect(() => {
      nbits(null);
    }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => {
      nbits(undefined);
    }).toThrowError(TypeError);
  });
  it('should throw a TypeError', () => {
    expect(() => {
      nbits('banana');
    }).toThrowError(TypeError);
  });
  it('should return 0', () => {
    expect(nbits(0)).toEqual(0);
  });
  it('should return 1', () => {
    expect(nbits(1)).toEqual(1);
  });
  it('should return 1', () => {
    expect(nbits(2)).toEqual(1);
  });
  it('should return 3', () => {
    expect(nbits(7)).toEqual(3);
  });
  it('should return 7', () => {
    expect(nbits(2017)).toEqual(7);
  });
  it('should return 7', () => {
    expect(nbits(2018)).toEqual(7);
  });
  it('should return 11', () => {
    expect(nbits(2047)).toEqual(11);
  });
  it('should return 1', () => {
    expect(nbits(4096)).toEqual(1);
  });
  it('should return 23', () => {
    expect(nbits(0x7afafafa)).toEqual(23);
  });
  it('should return 28', () => {
    expect(nbits(0xfffffff)).toEqual(28);
  });
  it('should return 31', () => {
    expect(nbits(0x7fffffff)).toEqual(31);
  });
  it('should return 32', () => {
    expect(nbits(0xffffffff)).toEqual(32);
  });
  it('should return 32', () => {
    expect(nbits(-1)).toEqual(32);
  });
  it('should return 31', () => {
    expect(nbits(-2)).toEqual(31);
  });
  it('should return 31', () => {
    expect(nbits(-3)).toEqual(31);
  });
  it('should return 30', () => {
    expect(nbits(-4)).toEqual(30);
  });
});

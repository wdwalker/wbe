# Simple Exercises

This repo contains simple coding exercises contained within a structured [Node.js](https://nodejs.org/en/) and [NPM](https://www.npmjs.com) development environment. Included in this development environment are the following:

* Use of [Babel](https://babeljs.io) to transpile ES6
* Adherence to the [AirBnB style guide](https://www.npmjs.com/package/eslint-config-airbnb)
* Use of [Jest](https://facebook.github.io/jest/) for testing and code coverage analysis
* Use of git pre-commit hooks to ensure tests run before commits are permitted

Doing an `npm install` will install the dependencies, eslint the code, and run the unit tests. Test coverage will be presented on the console and can also be viewed by opening `./coverage/lcov-report/index.html` in your browser.


## fib
Fib is a simple Fibonacci sequence utility: pass in an index and it will return the Fibonacci number at that index.

## nbits
Nbits counts the number of bits set to 1 in a non-negative integer.

## indexOf
IndexOf find the index of a value in an array of values.

## sudoku
A simple sudoku puzzle solver that uses the backtracking algorithm.